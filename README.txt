Introduction:
-------------
* This module create language proficiency field type to manage user language
  skills. It provide 3 fields to store language name, mode and proficiency.

* This language proficiency field type is possible to use in job portals,
  application forms and user registration to capture the expertise in language.

* For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/raj_visu/2565451

Requirements:
------------
Fields module.


Recommended modules:
--------------------
None.


Installation:
-------------
* Install as usual, see http://drupal.org/node/895232 for further information.

CONFIGURATION
------------
1. Install your module as usual. See more details on:
   https://drupal.org/documentation/install/modules-themes/modules-7
2. Go to /admin/structure/types/manage/<content-type>/fields and set field type
   as language proficiency.
3. If you need multiple values for this, set unlimited under "Number of values".
